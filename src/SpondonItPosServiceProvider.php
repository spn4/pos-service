<?php

namespace SpondonIt\PosService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\PosService\Middleware\PosService;

class SpondonItPosServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(PosService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'uxpos');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'pos');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
