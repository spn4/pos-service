<?php
return array(
    "welcome" => "Welcome",
    "welcome_title" => "Welcome To UXSEVEN",
"admin_setup" => "Admin Setup",
"done_msg" => "Installation done. You can now login.",
"email" => "Email",
"password" => "Password",
"password_confirmation" => "Confirm Password",
'with_demo_data' => 'Install With Demo Data',
"welcome_description" => "Thank you for choosing POS MANAGER for online Biz. Please follow the steps to complete POS installation!",
);
